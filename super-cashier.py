"""Import module tabulate, kita gunakan untuk membuat table agar terlihat rapih"""
from tabulate import tabulate

"""
Membuat Class Transaction dengan memiliki beberapa method seperti:
1. addItem = method untuk menambahkan item baru
2. updateItemName = method untuk mengupdate itemName/Nama Produk
3. updateItemQuantity = method untuk mengupdate quantity/jumlah produk
4. updateItemPrice = method untuk mengupdate itemPrice/Harga Produk
5. deleteItem = method untuk menghapus 1 item
5. resetItem = method untuk menghapus semua item
6. checkOrder = method untuk menampilkan item apa saja yang sudah di keranjang
7. totalOrder = method untuk menjumlahkan seluruh item yang ada di keranjang dan menyelesaikan pesanan kamu
"""
class Transaction:
    def __init__(self):
        """
        variable header untuk header table, dan cart untuk menyimpan item di keranjang
        """
        self.header=[['Nama Item','Jumlah Item','Harga/Item','Harga Total']] 
        self.cart= []

    def addItem(self):
        """
        pada method addItem ini menggunakan input form dan perulangan while 
        dan untuk mengatasi kesalahan input data menggunakan try except valueError
        """
        while True:
            try:
                itemName = str(input('Nama Produk: '))
                quantity = int(input('Jumlah Produk: '))
                price = int(input('Harga Produk: '))
                self.cart.append([itemName,quantity,price,quantity*price])
                break
            except ValueError:
                print("Data yang anda masukkan tidak valid / null(kosong), silahkan isi kembali...")
        print('Data berhasil ditambahkan.\n')
        
    def updateItemName(self):
        """
        pada method updateItemName ini menggunakan input form 'name' sebagai primary key dan 'itemName' untuk menyimpan data nama produk yang baru
        """
        x = 0 #x bernilai 0 untuk mulai nya perulangan for pada method ini
        name = str(input('Masukkan Nama Produk yang ingin diupdate namanya: '))
        """
        for disini untuk mencari apakah ada data nama yg sesuai dengan variable 'name' yang kita input
        dan ditambahkan logika if apabila data nya tidak sesuai dengan data pada cart/keranjang maka data tidak ditemukan/tidak ada aksi apapun
        """
        for x in range(len(self.cart)):
            if(self.cart[x][0]==name):
                """
                while disini juga untuk melakukan pengulangan berulang untuk mendapatkan data yang valid
                """
                while True:
                    try:
                        itemName = str(input(f'Update Nama: {name}: '))
                        self.cart[x][0]=itemName
                        break
                    except ValueError:
                        print("Data yang anda masukkan tidak valid / null(kosong), silahkan isi kembali...")
                print(f'Data nama: {name} berhasil diupdate.\n')
                break
        else:
            print('Nama Produk tidak ditemukan!')
    
    def updateItemQuantity(self):
        """
        pada method updateItemQuantity ini menggunakan input form 'name' sebagai primary key dan 'quantity' untuk menyimpan data jumlah produk yang baru
        """
        x = 0 #x bernilai 0 untuk mulai nya perulangan for pada method ini
        name = str(input('Masukkan nama produk yang ingin diupdate stoknya: '))
        """
        for disini untuk mencari apakah ada data nama yg sesuai dengan variable 'name' yang kita input
        dan ditambahkan logika if apabila data nya tidak sesuai dengan data pada cart/keranjang maka data tidak ditemukan/tidak ada aksi apapun
        """
        for x in range(len(self.cart)):
            if(self.cart[x][0]==name):
                """
                while disini juga untuk melakukan pengulangan berulang untuk mendapatkan data yang valid
                """
                while True:
                    try:
                        quantity = int(input(f'Update Stok: {name}: '))
                        """
                        if disini untuk mencegah input data bernilai 0
                        """
                        if(quantity==0):
                            print('Data yang di input tidak boleh bernilai 0')
                            continue
                        self.cart[x][1]=quantity
                        self.cart[x][3]=quantity*self.cart[x][2]
                        break
                    except ValueError:
                        print("Data yang anda masukkan tidak valid / null(kosong), silahkan isi kembali...")
                print(f'Data jumlah stok produk: {name} berhasil diupdate.\n')
                break
        else:
            print('Data nama tidak ditemukan! Silahkan isi kembali...')
    
    def updateItemPrice(self):
        """
        pada method updateItemPrice ini menggunakan input form 'name' sebagai primary key dan 'price' untuk menyimpan data harga produk yang baru
        """
        x = 0 #x bernilai 0 untuk mulai nya perulangan for pada method ini
        name = str(input('Masukkan Nama Produk yang ingin diupdate harganya: '))
        """
        for disini untuk mencari apakah ada data nama yg sesuai dengan variable 'name' yang kita input
        dan ditambahkan logika if apabila data nya tidak sesuai dengan data pada cart/keranjang maka data tidak ditemukan/tidak ada aksi apapun
        """
        for x in range(len(self.cart)):
            if(self.cart[x][0]==name):
                """
                while disini juga untuk melakukan pengulangan berulang untuk mendapatkan data yang valid
                """
                while True:
                    try:
                        price = int(input(f'Update harga: {name}: '))
                        """
                        if disini untuk mencegah input data bernilai 0
                        """
                        if(price==0):
                            print('Data yang di input tidak boleh bernilai 0')
                            continue
                        self.cart[x][2]=price
                        self.cart[x][3]=price*self.cart[x][1]
                        break
                    except ValueError:
                        print("Data yang anda masukkan tidak valid/null(kosong), silahkan isi kembali...")
                print(f'Data harga produk: {name} berhasil diupdate.\n')
                break
        else:
            print('Data nama tidak ditemukan! Silahkan isi kembali...')
    
    def deleteItem(self):
        """
        pada method deleteItem ini menggunakan input form 'name' sebagai primary key untuk menghapus 1 item tersebut
        """
        x = 0 #x bernilai 0 untuk mulai nya perulangan for pada method ini
        name = str(input('Masukkan Nama Produk yang ingin dihapus: '))
        """
        for disini untuk mencari apakah ada data nama yg sesuai dengan variable 'name' yang kita input
        dan ditambahkan logika if apabila data nya tidak sesuai dengan data pada cart/keranjang maka data tidak ditemukan/tidak ada aksi apapun
        """
        for x in range(len(self.cart)):
            if(self.cart[x][0]==name):
                self.cart.pop(x)
                print(f'Produk : {name} berhasil dihapus!.\n')
                break
        else:
            print('Data nama tidak ditemukan! Silahkan isi kembali...')
        
    def resetItem(self):
        """
        pada method resetItem ini untuk menghapus / reset data pada cart / keranjang
        """
        self.cart.clear()
        print('Data Berhasil Di Reset!\n')
    
    def checkOrder(self):
        """
        pada method checkOrder untuk mengecek / menampilkan semua data pada keranjang
        """
        table = self.header + self.cart
        print(tabulate(table))
    
    def totalOrder(self):
        """
        pada method totalOrder ini untuk menjumlahkan semua order yang telah diinput sekaligus mengakhiri pesanan
        dengan menampilkan semua detail pesanan dengan memanggil method checkOrder dan total harga beserta diskon(jika ada)
        dan mereset semua transaksi karna dianggap telah menyelesaikan transaksi 
        """
        x = 1
        """
        for dibawah ini untuk menjumlahkan data pada kolom total harga dan
        menyimpan kedalam variable totalTransaction
        variable totalBefore dibuat untuk menyimpan nilai total harga sebelum diskon(jika ada)
        """
        total = [] 
        for x in range(len(self.cart)):
            total.append(self.cart[x][3])  

        totalTransaction = sum(total) #fungsi sum untuk menjumlahkan semua data pada total
        totalBefore=totalTransaction

        """
        logika if disini untuk menentukan customer akan mendapatkan diskon atau tidak, penjelasan nya adalah:
        1. Jika customer belanja lebih dari atau sama dengan Rp. 500.000 maka akan mendapatkan diskon 10%
        2. Jika customer belanja lebih dari atau sama dengan Rp. 300.000 maka akan mendapatkan diskon 8%
        3. Jika customer belanja lebih dari atau sama dengan Rp. 200.000 maka akan mendapatkan diskon 5%
        """
        if(totalTransaction>=500000):
            totalDisc = totalTransaction*0.1
            totalTransaction = totalTransaction*0.9
        elif(totalTransaction>=300000):
            totalDisc = totalTransaction*0.08
            totalTransaction = totalTransaction*0.92
        elif(totalTransaction>=200000):
            totalDisc = totalTransaction*0.05
            totalTransaction = totalTransaction*0.95

        """
        logika if disini untuk mengkonfirmasi pesanan apakah sudah selesai atau masih ingin berbelanja
        """
        confirm = input(f'Total Belanja Anda Adalah Rp. {totalTransaction:,.2f} || Apakah Pesanan Anda sudah benar dan ingin melakukan pembayaran?y/n ')
        if(confirm=='y' or confirm=='Y'):
            """
            menambahkan variable name untuk menyimpan nama customer, lalu memanggil method checkOrder untuk menampilkan semua data pada cart / keranjang
            """
            name = str(input('Masukkan Nama Anda = '))
            print(f'Halo, {name}')
            print('Berikut Detail Pesanan Kamu: ')
            self.checkOrder()
            """
            logika if disini untuk mengecek apakah mendapatkan diskon atau tidak
            """
            if(totalBefore>=200000):
                print(f'Total         = Rp. {totalBefore:,.2f}')
                print(f'Total Diskon  = Rp. {totalDisc:,.2f}')
            print(f'Total Belanja = Rp. {totalTransaction:,.2f}')
        elif(confirm=='n' or confirm=='N'):
            print('Silahkan tambahkan/edit item belanjaan anda kembali.')
        else:
            print('Data yang anda input tidak valid!')


"""TEST CASE"""
trnsct_123 = Transaction()
"""Test 1 Tambah Data"""
print('----Test 1 Tambah Data----')
trnsct_123.addItem()
trnsct_123.addItem()
trnsct_123.addItem()
trnsct_123.checkOrder()
print('----Test 1 Selesai----\n\n')

"""Test 2 Update Data"""
print('----Test 2 Update Data----')
trnsct_123.updateItemQuantity()
trnsct_123.updateItemPrice()
trnsct_123.updateItemName()
trnsct_123.checkOrder()
print('----Test 2 Selesai----\n\n')


"""Test 3 Delete Data"""
print('----Test 3 Delete Data----')
trnsct_123.deleteItem()
trnsct_123.checkOrder()
print('----Test 3 Selesai----\n\n')

"""Test 4 Total Pesanan Selesai"""
print('----Test 4 Total Pesanan Selesai----')
trnsct_123.totalOrder()
print('----Test 4 Selesai----\n\n')

"""Test 5 Reset Transaksi"""
print('----Test 5 Reset Transaksi----')
trnsct_123.resetItem()
trnsct_123.checkOrder()
print('----Test 5 Selesai----\n')



