# Project Super Cashier
Program Super Cashier sederhana menggunakan bahasa pemograman python.

# Latar Belakang Problems
Masalah dalam membuat program ini adalah kita harus membuat program super cashier dimana program tersebut membutuhkan data cart untuk menyimpan data, tapi disini kita tidak menggunakan database sebagai penyimpanan datanya. Program ini juga harus memilihi response error yang baik (defensive programming)

# Requirements yang dibutuhkan:
1. Menggunakan List untuk menyimpan data pada cart/keranjang.
2. Menggunakan module tabulate untuk mendesign cart / keranjang
3. Memahami fungsi-fungsi pada List seperti:
    1. append untuk menambahkan data pada list
    2. nama_list[urutan_data] untuk mengupdate data pada list
    3. pop(urutan_data)  untuk menghapus 1 row data pada list
    4. clear() untuk menghapus semua data pada list
4. Memahami menggunakan For Loop terutama dalam List
5. Memahami While Loop dan Try Catch Exception untuk response error yang baik

# Flow Chart
 ![Flow Chart Super Cashier](img/flowchart-project-python.png)
1. Program dimulai dengan menambahkan data dengan menjalankan fungsi addItem()
2. Setelah itu, kita bisa melakukan berbagai fungsi seperti:
    1. Update Data dengan function updateItemName untuk item nama, updateItemQuantity untuk item stok/jumlah, updateItemPrice untuk item harga
    2. Delete Data dengan memanggil nama produk sebagai primary key
    3. Add Data kita bisa menjalankan fungsi addItem lagi apabila ada produk yang ingin ditambahkan
    4. Check Order untuk melihat cart/keranjang belanjaan kita
    5. Reset Item untuk menghapus semua data pada cart/keranjang
3. Step selanjutnya masuk ke fungsi totalOrder() untuk mengkonfirmasi pesanan kita sudah selesai atau belum, jika sudah bisa ketik="y" untuk menyelesaikan pesanan dan ketik="n" untuk kembali ke step no. 2

# Penjelasan Code
Untuk penjelasan code sudah didalam [super-cashier.py](super-cashier.py) pada tiap baris kode nya

# Cara Menjalankan Program
1. Download semua file/module Python ke dalam satu direktori lokal.
2. Buka terminal dan sesuaikan lokasi direktori lokal.
3. Anda cukup menjalankan module python super-cashier.py, program ini defaultnya akan menjalankan:
    1. menambahkan data sebanyak 3 item
    2. update data nama, jumlah stok dan harga.
    3. delete 1 item
    4. mengkonfirmasi pesanan apakah sudah sesuai atau belum
    5. reset transaksi

# Hasil Test Case
1. Menambahkan Data\
    ![Test Case 1](img/test1_tambah_data.JPG)

2. Mengupdate Data Nama, Stok dan Harga\
    ![Test Case 2](img/test2_update_data.JPG)
    
3. Menghapus 1 Data\
    ![Test Case 3](img/test3_delete_data.JPG)

4. Menyelesaikan Pesanan\
    ![Test Case 4](img/test4_total_order.JPG)

5. Reset Transaksi\
    ![Test Case 5](img/test5_reset_transaksi.JPG)


# Conclusion
Dari project ini saya menyadari masih kurang nya penggunaan module, terlebih dari keterbasan waktu untuk mempelajari module-module yang ada dalam bahasa pemograman python. program ini sudah saya buat interaktif karna kedepannya jika ada waktu dan sdm lebih saya ingin kembangkan menjadi bener2 aplikasi super cashier berbasis web yang terhubung dengan database seperti sql dengan menjadikan python ini sebagai backend aplikasi nya, untuk frontend nya saya kembangkan menggunakan python juga dengan memanfaatkan django atau bisa backend nya dijadikan API agar bisa digunakan untuk bahasa pemograman lainnya 
